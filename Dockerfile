############################
# STEP 1 build executable binary
############################
FROM golang:alpine AS builder
# Install git.
# Git is required for fetching the dependencies.
RUN apk update && apk add --no-cache git
# Build MUST provide the GOPKG build arg
# This is the path udner $GOPATH/src where your sources should be copied
ARG GOPKG=gopkg-unknown
# copy current dir, cd to it, download dependencies, and run build
ADD . /go/src/${GOPKG}
WORKDIR /go/src/${GOPKG}
RUN go get -d -v
# Build the binary (fully self-contained)
RUN CGO_ENABLED=0 go build -v -a -o /main
############################
# STEP 2 build a small image
############################
FROM scratch
# Copy our static executable.
COPY --from=builder /main /main
# Host should provide a volume from where the web server will serve static assets
VOLUME /public-html
# Run the hello binary.
ENTRYPOINT ["/main"]