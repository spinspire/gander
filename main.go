package main

import (
	"net/http"

	"gander/resources"

	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
)

func main() {
	// Default router with logger and recovery middleware enabled.
	r := gin.Default()
	// Serve frontend static files.
	r.Use(static.Serve("/", static.LocalFile("./public-html", true)))

	// Sample REST route.
	r.GET("/hello", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "hello",
		})
	})
	resources.RegisterRoutes(r)
	// Listen and serve.
	r.Run()
}
