package resources

import (
	"gander/persistence"
	"net/http"
	"reflect"

	"github.com/gin-gonic/gin"
	rdb "gopkg.in/rethinkdb/rethinkdb-go.v5"
)

// Resource represents a group of HTTP endpoints that support GET/POST/PUT/etc to store & retrieve a resource
type Resource struct {
	t   persistence.Table // Table that backs this resource
	typ reflect.Type      // type of the data the resource represents
}

// NewResource creates a new Resource bound to the given path under the given RouterGroup and backed by the named table
func NewResource(group *gin.RouterGroup, session *rdb.Session, path string, tableName string, example interface{}) *Resource {
	r := new(Resource)
	// store zero value of the resource value type
	r.typ = reflect.TypeOf(example)
	// create
	r.t = persistence.Table{Name: tableName, Session: session}
	r.t.TableCreate() // in case the table does not exist
	group.GET(path, r.LIST)
	group.GET(path+"/:id", r.GET)
	group.POST(path, r.POST)
	group.PUT(path+"/:id", r.PUT)
	return r
}

func (r *Resource) LIST(c *gin.Context) {
	var rows []interface{}
	r.t.All(&rows)
	c.JSON(http.StatusOK, gin.H{
		"result": rows,
	})
}

func (r *Resource) GET(c *gin.Context) {
	var row interface{}
	id := c.Param("id")
	if err := r.t.Get(id, &row); err == nil {
		c.JSON(http.StatusOK, gin.H{
			"result": row,
		})
	} else {
		c.JSON(http.StatusNotFound, gin.H{
			"error": err.Error(),
		})
	}
}

func (r *Resource) POST(c *gin.Context) {
	// this is some advanced reflection that I don't fully understand
	// see https://stackoverflow.com/questions/45679408/unmarshal-json-to-reflected-struct
	rowPtr := reflect.New(r.typ).Interface()
	if err := c.ShouldBindJSON(rowPtr); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if id, err := r.t.Insert(rowPtr); err == nil {
		c.JSON(http.StatusOK, gin.H{
			"result": id,
		})
	} else {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}
}

func (r *Resource) PUT(c *gin.Context) {
	// this is some advanced reflection that I don't fully understand
	// see https://stackoverflow.com/questions/45679408/unmarshal-json-to-reflected-struct
	rowPtr := reflect.New(r.typ).Interface()
	id := c.Param("id")
	if err := c.ShouldBindJSON(rowPtr); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	if count, err := r.t.Update(id, rowPtr); err == nil {
		if count >= 1 {
			c.JSON(http.StatusOK, gin.H{
				"result": count,
			})
		} else {
			c.JSON(http.StatusNotFound, gin.H{
				"result": count,
			})
		}
	} else {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}
}
